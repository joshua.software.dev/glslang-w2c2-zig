const std = @import("std");

const download = @import("src/download_wasm.zig");

fn build_glslang(
    builder: *std.Build,
    wasi: *std.Build.Step.Compile,
    temp_folder: []const u8,
    gen_file: []const u8,
    args: anytype,
) *std.Build.Step.Compile {
    const exe = builder.addExecutable(.{
        .name = "glslang",
        // In this case the main source file is merely a path, however, in more
        // complicated build scripts, this could be a generated file.
        .target = args.target,
        .optimize = args.optimize,
    });

    exe.strip = true;

    exe.linkLibC();
    exe.linkLibrary(wasi);
    exe.addIncludePath(.{ .path = "include/w2c2/2e9c86f70f2fcf3374d57020389f35ea082fc018/w2c2" });
    exe.addIncludePath(.{ .path = "include/w2c2/2e9c86f70f2fcf3374d57020389f35ea082fc018/wasi" });
    exe.addIncludePath(.{ .path = temp_folder });

    exe.addCSourceFile(.{ .file = .{ .path = gen_file }, .flags = &.{} });
    exe.addCSourceFile(.{ .file = .{ .path = "src/main.c" }, .flags = &.{} });

    return exe;
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const w2c2_dep = b.dependency("w2c2zig", .{ .target = target, .optimize = optimize });

    const download_wasm = b.step("download_wasm", "Download glslang.wasm.zst file if not already downloaded");
    download_wasm.makeFn = download.download_file;

    const run_w2c2 = b.addRunArtifact(w2c2_dep.artifact("w2c2"));
    run_w2c2.step.dependOn(download_wasm);
    var new_stdio = std.ArrayList(std.Build.Step.Run.StdIo.Check)
        .initCapacity(b.allocator, 1) catch @panic("oom");
    new_stdio.appendAssumeCapacity(.{ .expect_stderr_match = "" }); // silence stderr
    run_w2c2.stdio = .{ .check = new_stdio };
    run_w2c2.addFileArg(.{ .path = "src/glslang.wasm" });
    const temp_folder = b.makeTempPath();
    const gen_file = b.pathJoin(&.{ temp_folder, "glslang.c" });
    run_w2c2.addArg(gen_file);

    const glslang_exe = build_glslang(b, w2c2_dep.artifact("w2c2wasi"), temp_folder, gen_file, .{
        .target = target,
        .optimize = optimize
    });
    glslang_exe.step.dependOn(&run_w2c2.step);

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(glslang_exe);
}
