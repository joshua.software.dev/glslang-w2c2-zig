#include <stdio.h>
#include <stdlib.h>

#include "w2c2_base.h"
#include "wasi.h"
#include "glslang.h"

void
trap(
    Trap trap
) {
    fprintf(stderr, "TRAP: %s\n", trapDescription(trap));
    abort();
}


wasmMemory*
wasiMemory(
    void* instance
) {
    return glslang_memory((glslangInstance*)instance);
}

extern char** environ;


int main(int argc, char** argv) {
    glslangInstance instance;
    glslangInstantiate(&instance, NULL);

    if (!wasiInit(argc, argv, environ)) {
        fprintf(stderr, "failed to initialize WASI\n");
        return 1;
    }

    if (!wasiFileDescriptorAdd(-1, "/", NULL)) {
        fprintf(stderr, "failed to add preopen\n");
        return 1;
    }

    glslang__start(&instance);

    glslangFreeInstance(&instance);

    return 0;
}
