# glslang-w2c2-zig

A zig build system package for transforming a wasm32-wasi build of [glslang](https://github.com/joshua-software-dev/glslang) into a native binary in a build step using [w2c2](https://github.com/turbolent/w2c2).
